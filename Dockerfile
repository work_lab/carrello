FROM node

COPY package.json .
RUN npm install
COPY . .

ENV PUBLIC_URL http://40.115.56.240:3000
ENV REACT_APP_CMS_URL=http://jcmsb.hostinggratis.it
RUN npm run build
RUN npm run transpile

CMD PORT=$PORT npm run start:prod