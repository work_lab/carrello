import React from 'react';
import services from './services'
import axios from 'axios';
import Modal from 'react-modal';
import { Media,Button} from 'react-bootstrap';


const customStyles = {
    overlay: {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'rgba(61, 61, 61, 0.75)' 
    },
    content: {
      position: 'absolute',
      top: '50px',
      left: '230px',
      right: '230px',
      bottom: '65px',
      border: '1px solid #ccc',
      background: '#fff',
      overflow: 'auto',
      WebkitOverflowScrolling: 'touch',
      borderRadius: '4px',
      outline: 'none',
      padding: '20px'
    }
};

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
      prodotti:[]
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    

  }
  
  componentDidMount() {
    /*var carrello=window.localStorage.getItem("carrello");
    if(carrello){
      var carrelloVett=carrello.split("_");
      axios({
        method: 'post',
        headers: { 'Content-Type': 'application/json' },            
        url: process.env.REACT_APP_CMS_URL+'/cockpit-master/api/collections/get/prodotto?token=193f5bf31f4d33b0b06f76d90b462c',
        data: 
            JSON.stringify({
                filter: {_id:carrelloVett}
               
            })
      })
      .then(
          response=>{
              console.log("entriessss"+response.data.entries);
              this.setState({ prodotti: [...this.state.prodotti, response.data.entries] })
          }
      )
    }*/
    window.addEventListener('addToCart', (event) => {
        axios({
            method: 'post',
            headers: { 'Content-Type': 'application/json' },            
            url: process.env.REACT_APP_CMS_URL+'/cockpit-master/api/collections/get/prodotto?token=193f5bf31f4d33b0b06f76d90b462c',
            data: 
                JSON.stringify({
                    filter: {_id:event.detail}
                   
                })
          })
    .then(
        response=>{
            console.log("entriessss"+response.data.entries);
            this.setState({ prodotti: [...this.state.prodotti, response.data.entries] })
        }
    )
      console.log("evento arrivato"+event.detail);
    }, false);
    window.addEventListener('openCart', (event) => {
        //this.setState({ products: [...this.state.products, event.detail] });
       

 
        this.openModal();
        console.log("evento arrivato"+event.detail);
      }, false);
  }
  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  render() {
    var vet=this.state.prodotti;
    return (
      <div>
        
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
         {     
             
            
             vet.map(item=>{
                  return( <Item prod={item[0]}></Item>);
              }) 
            }      
            
        </Modal>
      </div>
    );
  }
}
class Item extends React.Component{
    constructor(props){
            super(props);
    }
     createMarkup(el) {
        return {__html: el};
      }
    render(){
        console.log("Item",this.props.prod);
        return(
            
            <Media>
            <Media.Left>
            <img width={64} height={64} src={process.env.REACT_APP_CMS_URL+""+this.props.prod.img.path} alt="thumbnail" />
            </Media.Left>
            <Media.Body>
            <Media.Heading>{this.props.prod.nome}</Media.Heading>
            <p>
             <div dangerouslySetInnerHTML={this.createMarkup(this.props.prod.caratteristiche)} />    
             prezzo: {this.props.prod.prezzoUs}
            </p>
            <Button bsStyle="success">Acquista</Button>
            <Button bsStyle="danger">Rimuovi</Button>
            </Media.Body>
    </Media>
        );
    }
    
}


export default App;